import csv
import json
import datetime
from pathlib import Path

t0 = datetime.datetime.strptime("2020/01/01 00:00:00", "%Y/%m/%d %H:%M:%S")
def makeTimeStampReversed(ts):
    return t0 + (t0 - ts)

eventlog_in = Path("/home/yesant/Documents/ProgrammingProjects/ProcessSequencePrediction/data/Road_Traffic_Fine_Management_Process.csv")
eventlog_out = Path("/home/yesant/Documents/ProgrammingProjects/ProcessSequencePrediction/data/Road_Traffic_Fine_Management_Process_reversed.csv")


csvfile_in = open('%s' % eventlog_in, 'r')
spamreader = csv.reader(csvfile_in, delimiter=',', quotechar='|')
header = next(spamreader, None)  # skip the headers

print(header)

writer = csv.writer(open(eventlog_out, 'w'))
writer.writerow(header)


l = list()
for i in spamreader:
    l.append(i)



for i in range(len(l)-1, 0, -1):
    timestamp = datetime.datetime.strptime(l[i][2], "%Y/%m/%d %H:%M:%S")

    changed = makeTimeStampReversed(timestamp)
    timestamp = changed.strftime("%Y/%m/%d %H:%M:%S")

    writer.writerow([l[i][0],l[i][1],timestamp,l[i][3]])

